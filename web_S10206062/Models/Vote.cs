﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace web_S10206062.Models
{
    public class Vote
    {
        [Display(Name = "Book ID")]
        public int BookId { get; set; }

        public string Justification { get; set; }
    }
}
